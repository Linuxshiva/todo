import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthService } from "angular2-social-login";
import { DashboardPage } from '../dashboard/dashboard';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

   htmlStr: string ;
   public user;
   sub: any;
   public inngarry = new Array();
   constructor(public navCtrl: NavController,public _auth: AuthService) {
     
    }
   
   signIn(a){
     //console.log(a);
     //alert("yes");
     this.sub = this._auth.login(a).subscribe(
       (data) => {
         console.log(data);
         this.user=data;
         console.log("logged in");
         localStorage.setItem("uname",this.user.name);
         this.navCtrl.push(DashboardPage);
        }
         
     )
   }
 
   logout(){
     this._auth.logout().subscribe(
       (data)=>{console.log(data);this.user=null;}
     )
   }

}
