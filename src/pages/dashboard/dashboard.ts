import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the DashboardPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  private isOpen= false;
  private tostr:string;
  private task;
  private date;
  private lguser;
  
  createToDo(){
    this.isOpen =true;
  }
  closetodo(){
    this.tostr ="<div class='gen'><div class='cardpr'><div class='crtil'>Today</div><div class='crdcnt'><ul><li class='lst'><p>"+this.task+"</p><p>"+this.date+"</p></li><//ul></div></div></div>"
    this.isOpen =false;
  }
  
  ionViewDidLoad() {
    this.lguser = localStorage.getItem("uname");
    console.log("User "+localStorage.getItem("uname")+" Entered");
  }

}
